%global __requires_exclude_from %{?__requires_exclude_from:%__requires_exclude_from|}/maven-metadata/javapackages-metadata.xml$
%global _python_bytecompile_extra 0

Name:           javapackages-tools
Version:        5.3.0
Release:        6
Summary:        Support for Java packaging with macros and scripts
License:        BSD-3-Clause
URL:            https://github.com/fedora-java/javapackages
Source0:        https://github.com/fedora-java/javapackages/archive/%{version}.tar.gz
Patch0000:      0001-Support-threads-and-skipTests-params.patch
Patch0001:	0001-make-glob-pattern-work-with-python3.10.patch
BuildArch:      noarch
BuildRequires:  coreutils which make asciidoc xmlto python3-devel python3-lxml
BuildRequires:  python3-setuptools python3-six
Requires:       javapackages-filesystem = %{version}-%{release} coreutils
Requires:       findutils which java-1.8.0-openjdk-headless
Provides:       jpackage-utils = %{version}-%{release} mvn(com.sun:tools) = SYSTEM
Provides:       mvn(sun.jdk:jconsole) = SYSTEM

%description
The javapackages-tools package contains macros and scripts to support Java packaging.

%package -n javapackages-filesystem
Summary:        Filesystem layout for java packages
Provides:       eclipse-filesystem = %{version}-%{release}
Obsoletes:      eclipse-filesystem < 2

%description -n javapackages-filesystem
The javapackages-filesystem package contains some basic directories into which
Java packages install their content.

%package -n maven-local
Summary:        Support macros and scripts for maven packaging
Requires:       %{name} = %{version}-%{release} javapackages-local = %{version}-%{release}
Requires:       xmvn-minimal xmvn-mojo xmvn-connector-aether
Requires:       mvn(org.apache.maven.plugins:maven-compiler-plugin)
Requires:       mvn(org.apache.maven.plugins:maven-jar-plugin)
Requires:       mvn(org.apache.maven.plugins:maven-javadoc-plugin)
Requires:       mvn(org.apache.maven.plugins:maven-resources-plugin)
Requires:       mvn(org.apache.maven.plugins:maven-surefire-plugin)
Requires:       mvn(junit:junit) mvn(org.apache.maven.surefire:surefire-junit4)
Requires:       mvn(org.apache.maven.surefire:surefire-testng)

%description -n maven-local
The maven-local package contains macros and scripts to support packaging Maven artifacts.

%package -n gradle-local
Summary:        Gradle's local mode
Requires:       %{name} = %{version}-%{release} javapackages-local = %{version}-%{release}
Requires:       gradle >= 2.2.1-2 xmvn-connector-gradle

%description -n gradle-local
The gradle-local package implements local mode for Gradle, which allows artifact resolution
when using XMvn resolver.

%package -n ivy-local
Summary:        Local mode for Apache Ivy
Requires:       %{name} = %{version}-%{release} javapackages-local = %{version}-%{release}
Requires:       apache-ivy >= 2.3.0-8 xmvn-connector-ivy

%description -n ivy-local
The ivy-local package implements local mode for Apache Ivy, which allows artifact
resolution when using XMvn resolver.

%package -n python3-javapackages
Summary:        Java packaging's Module for handling various files
Requires:       python3-lxml python3-six
Obsoletes:      python-javapackages < %{version}-%{release}

%description -n python3-javapackages
The python3-javapackages package is a module for handling, querying and manipulating
of various files for Java packaging in Linux distributions.

%package -n javapackages-local
Summary:        Support of java packaging with Non-essential macros and scripts
Requires:       %{name} = %{version}-%{release} xmvn-install xmvn-subst
Requires:       xmvn-resolve java-1.8.0-openjdk-devel
Requires:       python3-javapackages = %{version}-%{release} python3

%description -n javapackages-local
The javapackages-local package contains non-essential macros and scripts which supports
Java packaging.

%prep
%autosetup -n javapackages-%{version} -p1

%build
%configure --pyinterpreter=%{__python3} --rpmmacrodir=%{rpmmacrodir} \
           --default_jdk=%{?_root_prefix}%{!?_root_prefix:%{_prefix}}/lib/jvm/java-1.8.0-openjdk \
           --default_jre=%{?_root_prefix}%{!?_root_prefix:%{_prefix}}/lib/jvm/jre-1.8.0-openjdk
./build

%install
./install

sed -e 's/.[17]$/&*/' -i files-*

%check
. ./config.status
for test in test/java-functions/*_test.sh; do
    echo "`basename $test`:"
    sh $test
done

%files -f files-tools

%files -n ivy-local -f files-ivy

%files -n maven-local

%files -n gradle-local -f files-gradle

%files -n javapackages-filesystem -f files-filesystem

%files -n javapackages-local -f files-local

%files -n python3-javapackages -f files-python
%license LICENSE

%changelog
* Fri Mar 29 2024 Ge Wang <wang__ge@126.com> - 5.3.0-6
- Rebuild for next release

* Wed May 11 2022 Ge Wang <wangge20@h-partner.com> - 5.3.0-5
- License compliance rectification

* Fri Feb 11 2022 baizhonggui <baizhonggui@huawei.com> - 5.3.0-4
- Deprecate python-nose and fix python version not recognized

* Fri Apr 16 2021 maminjie <maminjie1@huawei.com> - 5.3.0-3
- Support threads and skipTests params

* Tue Dec 10 2019 yanzhihua4 <yanzhihua4@huawei.com> - 5.3.0-2
- Package init
